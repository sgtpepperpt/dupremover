#include "digest_map.h"

#include <stdlib.h>
#include <string.h>
#include <algorithm>

using namespace std;

bool compare_duplicates(duplicate_group* i, duplicate_group* j) {
    return i->get_file_size() > j->get_file_size();
}

void digest_map::insert(const unsigned char* key, const file_data* value) {
    if (I.find(key) == I.end()) {
        vector<const file_data*> paths;
        paths.push_back(value);

        I[key] = paths;
    } else {
        (I[key]).push_back(value);
    }
}

vector<duplicate_group*> digest_map::get_all(const int ignore_empty) {
    vector<duplicate_group*> duplicates;

    for (unordered_map<const unsigned char*, vector<const file_data*>, VoidHash, VoidEqual>::iterator it = I.begin(); it != I.end(); ++it) {
        //const unsigned char* curr_hash = it->first;
        const vector<const file_data*> curr_paths = it->second;

        if(curr_paths.size() > 1 && (!ignore_empty || (*(curr_paths.begin().base()))->size)) {
            vector<const file_data*> dups_list;

            // probably a duplicate, check if all files have the same size
            const size_t first_size = (*(curr_paths.begin().base()))->size;

            for(const file_data* f : curr_paths) {
                if(f->size == first_size)
                    dups_list.push_back(f);
            }

            if(dups_list.size() > 1) {
                duplicate_group* dup = new duplicate_group(first_size);
                dup->insert(dups_list);
                duplicates.push_back(dup);
            }
        }
    }

    std::sort(duplicates.begin(), duplicates.end(), compare_duplicates);
    return duplicates;
}