#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include "digest.h"

#include "digest_map.h"
#include "fs_util.h"
#include "util.h"

using namespace std;

int main() {
    vector<string> ignore_dirnames;
    ignore_dirnames.push_back(".git");
    ignore_dirnames.push_back(".local");
    ignore_dirnames.push_back(".wine");
    ignore_dirnames.push_back("snap");
    ignore_dirnames.push_back(".CLion2018.3");
    ignore_dirnames.push_back("Londres2018");
    ignore_dirnames.push_back("pepper");
    ignore_dirnames.push_back("uk2017pt2+barcelona2018");

    vector<string> ignore_paths;
    ignore_paths.push_back("/home/gb/visen-paper/");

    struct timeval start = curr_time();

    vector<file_data*> files = iterate_dirs("/home/gb/Desktop", ignore_dirnames, ignore_paths);

    // calculate digests
    digest_map hashes;
    size_t counter = 0;
    for(file_data* file : files) {
        if(counter++ % 1000 == 0)
            printf("Progress %d/%d\n", counter-1, files.size());


        unsigned char* digest = (unsigned char*)malloc(DIGEST_LEN);
        if(digest_file(file->path, digest))
            continue;

        /*for (int i = 0; i < EVP_MD_size(EVP_sha256()); ++i)
            printf("%02x", digest[i]);
        printf("\n");*/

        hashes.insert(digest, file);
    }

    printf("Calculated digests\n");

    // list duplicates
    int ignore_empty = 1;
    vector<duplicate_group*> duplicates = hashes.get_all(ignore_empty);

    for (duplicate_group* d : duplicates) {
        printf("%lu Duplicates (%.02fkb):\n", d->get_nr_files(), d->get_file_size() / 1024.0);

        for (size_t i = 0; i < d->get_nr_files(); ++i)
            printf("%s\n", d->get_paths()[i]);
        printf("\n");
    }

    double t = time_elapsed_ms(start, curr_time());

    printf("Total files iterated: %lu %lf\n", files.size(), t / 1000.0);
    return 0;
}
