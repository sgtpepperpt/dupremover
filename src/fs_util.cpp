#include "fs_util.h"

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <iostream>

using namespace std;

std::string make_path(const char* base_dir, const char* filename) {
    int has_slash = base_dir[strlen(base_dir) - 1] == '/';

    char path[strlen(base_dir) + has_slash + strlen(filename) + 1];
    strcpy(path, base_dir);

    if(!has_slash)
        path[strlen(base_dir)] = '/';

    strcpy(path + strlen(base_dir) + !has_slash, filename);
    path[strlen(base_dir) + !has_slash + strlen(filename)] = '\0';

    return std::string(path);
}

int is_ignored_path(const char* name, const vector<string> ignore_paths) {
    for(string path : ignore_paths) {
        if(!strcmp(name, path.c_str()))
            return 1;
    }

    return 0;
}

int is_ignored_dir(const char* name, const vector<string> ignore_dirnames) {
    if (!strcmp(name, ".") || !strcmp(name, ".."))
        return 1;

    for(string dirname : ignore_dirnames) {
        if(!strcmp(name, dirname.c_str()))
            return 1;
    }

    return 0;
}

std::vector<file_data*> iterate_dirs(const char* base_dir, const vector<string> ignore_dirnames, const std::vector<std::string> ignore_paths) {
    vector<file_data*> files;

    DIR *dir;
    struct dirent *entry;

    if((dir = opendir(base_dir))) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_DIR) {
                const char* dir_name = entry->d_name;

                // ignore based on dir name (relative)
                if(is_ignored_dir(dir_name, ignore_dirnames))
                    continue;

                string path = make_path(base_dir, dir_name);
                cout << path <<endl;

                // ignore path (absolute)
                if(is_ignored_path(path.c_str(), ignore_paths))
                    continue;

                vector<file_data*> sub = iterate_dirs(path.c_str(), ignore_dirnames, ignore_paths);
                files.insert(files.end(), sub.begin(), sub.end());
            } else if (entry->d_type == DT_REG) {
                string path = make_path(base_dir, entry->d_name);

                file_data* f = (file_data*)malloc(sizeof(file_data));
                f->path = (char*)malloc(strlen(path.c_str()) + 1);
                strcpy(f->path, path.c_str());

                f->size = get_file_size(f->path);

                files.push_back(f);
            }
        }
        closedir(dir);
    }

    return files;
}

size_t get_file_size(const char* filename) {
    FILE* fileptr = fopen(filename, "rb");
    fseek(fileptr, 0, SEEK_END);
    return (size_t )ftell(fileptr);
}

/*
void read_file(const char* filename) {
    FILE* fileptr = fopen(filename, "rb");  // Open the file in binary mode
    fseek(fileptr, 0, SEEK_END);          // Jump to the end of the file
    long filelen = ftell(fileptr);             // Get the current byte offset in the file
    rewind(fileptr);                      // Jump back to the beginning of the file

    char* buffer = (char *)malloc((filelen+1)*sizeof(char)); // Enough memory for file + \0
    fread(buffer, filelen, 1, fileptr); // Read in the entire file
    fclose(fileptr); // Close the file
}
*/