#ifndef DUPREMOVER_DIGEST_MAP_H
#define DUPREMOVER_DIGEST_MAP_H

#include <vector>
#include <list>
#include <unordered_map>
#include "map_util.h"
#include "duplicate_group.h"
#include "fs_util.h"

class digest_map {
public:
    void insert(const unsigned char* key, const file_data* value);
    std::vector<duplicate_group*> get_all(const int ignore_empty);
private:
    std::unordered_map<const unsigned char*, std::vector<const file_data*>, VoidHash, VoidEqual> I;
};

#endif