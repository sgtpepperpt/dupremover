#ifndef DUPREMOVER_FSUTIL_H
#define DUPREMOVER_FSUTIL_H

#include <string>
#include <vector>

typedef struct file_data {
    char* path;
    size_t size;
} file_data;

std::string make_path(const char* base_dir, const char* filename);
std::vector<file_data*> iterate_dirs(const char* base_dir, const std::vector<std::string> ignore_dirnames, const std::vector<std::string> ignore_paths);

size_t get_file_size(const char* filename);

#endif