#include "duplicate_group.h"

using namespace std;

duplicate_group::duplicate_group(size_t file_size) {
    this->file_size = file_size;
}

void duplicate_group::insert(vector<const file_data*> files) {
    this->nr_files = files.size();

    this->paths = (char**)malloc(sizeof(char*) * this->nr_files);
    for(size_t i = 0; i < files.size(); i++)
        this->paths[i] = files.at(i)->path;
}

const char* const * const duplicate_group::get_paths() {
    return this->paths;
}

const size_t duplicate_group::get_nr_files() {
    return this->nr_files;
}

const size_t duplicate_group::get_file_size() {
    return this->file_size;
}