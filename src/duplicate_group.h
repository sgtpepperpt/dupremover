#ifndef DUPREMOVER_DUPLICATE_GROUP_H
#define DUPREMOVER_DUPLICATE_GROUP_H

#include <stdlib.h>
#include <stdlib.h>

#include "fs_util.h"

class duplicate_group {
public:
    duplicate_group(size_t file_size);
    void insert(std::vector<const file_data*> files);
    const char* const * const get_paths();
    const size_t get_nr_files();
    const size_t get_file_size();
private:
    size_t file_size = 0;
    size_t nr_files = 0;
    char** paths = NULL;
};

#endif
