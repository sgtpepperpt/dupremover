#ifndef DUPREMOVER_DIGEST_H
#define DUPREMOVER_DIGEST_H

#include <stdlib.h>

extern const int DIGEST_LEN;

int digest_file(const char* filename, unsigned char* digest);

#endif