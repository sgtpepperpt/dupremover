#include "digest.h"

#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/err.h>

// digest implementation
const EVP_MD* HASH = EVP_sha512();

// length of digest, derived from above
const int DIGEST_LEN = EVP_MD_size(HASH);

// just for file reading
const size_t FILE_READ_CHUNK_SIZE = 256;

int digest_file(const char* filename, unsigned char* digest)
{
    EVP_MD_CTX *mdctx;
    if(!(mdctx = EVP_MD_CTX_create())) {
        printf("error!\n");
        exit(1);
    }

    if(1 != EVP_DigestInit_ex(mdctx, HASH, NULL)) {
        printf("error!\n");
        exit(1);
    }

    FILE* fileptr = fopen(filename, "rb");
    if(!fileptr) {
        printf("File open error %s, continue...\n", filename);
        return 1;
    }

    unsigned char* buffer[FILE_READ_CHUNK_SIZE];
    size_t read = 0;
    do {
        read = fread(buffer, 1, FILE_READ_CHUNK_SIZE, fileptr);
        if(1 != EVP_DigestUpdate(mdctx, buffer, read)) {
            printf("error!\n");
            exit(1);
        }
    } while (read == FILE_READ_CHUNK_SIZE);

    fclose(fileptr);

    // calculate and return result
    unsigned digest_len = 0;

    //digest = (unsigned char*)OPENSSL_malloc(EVP_MD_size(EVP_sha256()));

    if(1 != EVP_DigestFinal_ex(mdctx, digest, &digest_len)) {
        printf("error!\n");
        exit(1);
    }

    EVP_MD_CTX_destroy(mdctx);

    return 0;
}