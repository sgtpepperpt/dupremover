#ifndef DUPREMOVER_UTIL_H
#define DUPREMOVER_UTIL_H

struct timeval curr_time();
double time_elapsed_ms(struct timeval start, struct timeval end);

#endif
