#include "util.h"

#include <sys/time.h>
#include <time.h>

struct timeval curr_time() {
    struct timeval curr;
    gettimeofday(&curr, NULL);
    return curr;
}

double time_elapsed_ms(struct timeval start, struct timeval end) {
    long secs_used, micros_used;

    secs_used = (end.tv_sec - start.tv_sec); //avoid overflow by subtracting first
    micros_used = ((secs_used*1000000) + end.tv_usec) - (start.tv_usec);
    return (double)micros_used / 1000.0;
}